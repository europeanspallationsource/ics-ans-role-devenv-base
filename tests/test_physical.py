import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    '.molecule/ansible_inventory').get_hosts('physical_group')


def test_chronyd_running_and_enabled(Service):
    service = Service("chronyd")
    assert service.is_running
    assert service.is_enabled
