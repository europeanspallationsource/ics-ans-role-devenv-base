import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    '.molecule/ansible_inventory').get_hosts('all')


def test_basic_packages(Package):
    for package in ('git', 'vim-enhanced', 'tmux', 'screen', 'avrdude',
                    'picocom', 'net-snmp'):
        assert Package(package).is_installed


def test_firewalld_running_and_enabled(Service):
    service = Service("firewalld")
    assert service.is_running
    assert service.is_enabled


def test_devenv_setup_enabled(Service):
    service = Service("devenv-setup")
    assert service.is_enabled


def test_devenv_config(Command):
    cmd = Command('sudo /usr/local/bin/devenv-config -h')
    assert '--setup' in cmd.stdout


def test_ntpd_not_installed(Package):
    assert not Package("ntpd").is_installed
