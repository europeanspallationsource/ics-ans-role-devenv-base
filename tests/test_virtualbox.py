import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    '.molecule/ansible_inventory').get_hosts('virtualbox_group')


def test_chronyd_not_installed(host):
    assert not host.package("chronyd").is_installed


def test_usedns_set_to_no_in_sshd_config(host):
    # UseDNS setto no
    assert host.run('sudo grep -c "^UseDNS no" /etc/ssh/sshd_config').stdout == '1'
    # UseDNS only defined once
    assert host.run('sudo grep -c "^UseDNS" /etc/ssh/sshd_config').stdout == '1'
